using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SC.BL.Domain;

namespace SC.DAL.EF;

public class SupportCenterDbContext : DbContext
{
    public DbSet<Ticket> Tickets { get; set; }
    public DbSet<HardwareTicket> HardwareTickets { get; set; }
    public DbSet<TicketResponse> TicketResponses { get; set; }

    public SupportCenterDbContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
            optionsBuilder.UseSqlite("Data Source=SupportCenterDb_EFCodeFirst.db");
            
        // configure logging: write to debug output window
        optionsBuilder.LogTo(message => Debug.WriteLine(message), LogLevel.Information);
            
        // configure lazy-loading
        optionsBuilder.UseLazyLoadingProxies(false); // 'false' to disable -> default 'true'
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // 'Ticket.State' as index
        modelBuilder.Entity<Ticket>().HasIndex(t => t.State);
            
        // foreign key-name "TicketFK" in TicketResponse to Ticket
        modelBuilder.Entity<TicketResponse>().Property<int>("TicketFK"); // shadow property
        modelBuilder.Entity<TicketResponse>()
            .HasOne(tr => tr.Ticket)
            .WithMany(t => t.Responses)
            .HasForeignKey("TicketFK")
            .IsRequired();
    }
    
    public bool CreateDatabase(bool dropDatabase)
    {
        if (dropDatabase)
            Database.EnsureDeleted();

        return Database.EnsureCreated();
    }
}