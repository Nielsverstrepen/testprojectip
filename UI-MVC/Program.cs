using Microsoft.EntityFrameworkCore;
using SC.BL;
using SC.DAL;
using SC.DAL.EF;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("Your_PostgreSQL_Connection_String_Here") ?? throw new InvalidOperationException("Connection string 'CarsDbContextConnection' not found.");




// Add services to the container.
builder.Services.AddDbContext<SupportCenterDbContext>(optionsBuilder => 
    optionsBuilder.UseNpgsql(connectionString));
builder.Services.AddScoped<ITicketRepository, TicketRepository>();
builder.Services.AddScoped<ITicketManager, TicketManager>();
builder.Services.AddControllersWithViews();

// Change datetime properties to UTC
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

var app = builder.Build();

InitializeDatabase(dropDatabase:false);

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();


#region Method for initializing dummy data
void InitializeDatabase(bool dropDatabase)
{
    using (IServiceScope scope = app.Services.CreateScope())
    {
        SupportCenterDbContext dbCtx = scope.ServiceProvider.GetRequiredService<SupportCenterDbContext>();
        if (dbCtx.CreateDatabase(dropDatabase))
            DataSeeder.Seed(dbCtx);
    }
}
#endregion