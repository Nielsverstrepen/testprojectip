using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using SC.UI.Web.MVC.Models;

namespace SC.UI.Web.MVC.Controllers;

public class TicketController : Controller
{
    private readonly ITicketManager _mgr;

    public TicketController(ITicketManager ticketManager)
    {
        _mgr = ticketManager;
    }
    
    // GET: /Ticket
    public IActionResult Index()
    {
        IEnumerable<Ticket> tickets = _mgr.GetTickets();
        return View(tickets);
    }
    
    // GET: /Ticket/Details/<ticket_number>
    public IActionResult Details(int id)
    {
        Ticket ticket = _mgr.GetTicket(id);
        
        if(ticket.Responses != null)
            ViewBag.Responses = ticket.Responses;
        else
            ViewBag.Responses = _mgr.GetTicketResponses(ticket.TicketNumber);
        
        return View(ticket);
    }
    
    // GET: /Ticket/Edit/<ticket_number>
    public IActionResult Edit(int id)
    {
        Ticket ticket = _mgr.GetTicket(id);
        return View(ticket);
    }

    // POST: /Ticket/Edit/<ticket_number>
    [HttpPost]
    public IActionResult Edit(int id, Ticket ticket)
    {
        /*
        _mgr.ChangeTicket(ticket);
        return View(ticket);
        */
        
        if(!ModelState.IsValid)
            return View(ticket);
            
        _mgr.ChangeTicket(ticket);
        return RedirectToAction("Details", new {id = ticket.TicketNumber});
    }
    
    // GET: /Ticket/Create
    public IActionResult Create()
    {
        return View();
    }

    // POST: /Ticket/Create
    [HttpPost]
    /*
    public IActionResult Create(int accId, string problem)
    {
        Ticket newTicket = _mgr.AddTicket(accId, problem);
        return RedirectToAction("Details", new {id = newTicket.TicketNumber});
    }
    */
    public IActionResult Create(CreateTicketViewModel createTicket)
    {
        Ticket newTicket = _mgr.AddTicket(createTicket.AccId, createTicket.Problem);
        return RedirectToAction("Details", new {id = newTicket.TicketNumber});
    }
    
    // GET: /Ticket/Delete/<ticket_number>
    public IActionResult Delete(int id)
    {
        Ticket ticket = _mgr.GetTicket(id);
        return View(ticket);
    }

    // POST: /Ticket/Delete/<ticket_number>
    [HttpPost]
    public IActionResult DeleteConfirmed(int id)
    {
        _mgr.RemoveTicket(id);
        return RedirectToAction("Index");
    }
}