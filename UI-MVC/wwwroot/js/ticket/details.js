const loadResponsesButton = document.getElementById('loadResponses');
if (loadResponsesButton)
    loadResponsesButton.addEventListener('click', (event) => loadResponsesOfTicket());

function loadResponsesOfTicket() {
    const ticketNumberSpan = document.getElementById('ticketNumber');
    const ticketNumber = Number(ticketNumberSpan.innerText);

    fetch('/api/TicketResponses?ticketnumber='+ticketNumber, {
        method: 'GET',
        headers: { 'Accept': 'application/json' }
    })
        .then(response => { if (response.ok) return response.json(); })
        .then(data => showTicketResponses(data))
        .catch(error => alert('Oeps, something went wrong!'));
}
function showTicketResponses(responses) {
    const responsesTableBody = document.querySelector('#responses tbody').innerHTML = '';
    setTimeout(() => {
        responses.forEach(response => addResponseToList(response));
    }, 500);
}
function addResponseToList(response) {
    const date = new Date(response.date);
    const checked = response.isClientResponse ? 'checked="checked"' : '';
    const responsesTableBody = document.querySelector('#responses tbody');
    responsesTableBody.insertAdjacentHTML('beforeend', 
        `<tr>
             <td>${response.text}</td>
             <td>${date.toLocaleString(navigator.language)}</td>
             <td><input type="checkbox" disabled ${checked}/></td>
         </tr>`);
}

// nieuw antwoord toevoegen aan ticket
const createResponseButton = document.getElementById('createResponse');
if (createResponseButton)
    createResponseButton.addEventListener('click', (event) => createResponse());

function createResponse() {
    const ticketNumberSpan = document.getElementById('ticketNumber');
    const ticketNumber = Number(ticketNumberSpan.innerText);
    const answer = document.getElementById('responseText').value;
    const newResponse = {
        ticketNumber: ticketNumber,
        responseText: answer,
        isClientResponse: false
    };

    fetch('/api/TicketResponses', {
        method: 'POST',
        body: JSON.stringify(newResponse),
        headers : {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then(response => { if (response.ok) return response.json(); })
        .then(data => {
            addResponseToList(data);
            document.getElementById('responseText').value = '';
        })
        .catch(error => alert('Oeps, something went wrong!'));
}

// wijzig status van ticket naar 'Closed'
const closeTicketButton = document.getElementById('changeStatusToClosed');
if (closeTicketButton)
    closeTicketButton.addEventListener('click', (event) => closeTicket());

function closeTicket() {
    const ticketNumberSpan = document.getElementById('ticketNumber');
    const ticketNumber = Number(ticketNumberSpan.innerText);
    
    fetch('/api/Tickets/' + ticketNumber + '/State/Closed', {
        method: 'PUT'
    })
        .then(response => {
            if (response.ok)
                document.getElementById('state').innerText = 'Closed';
        })
        .catch(() => alert('Oeps, something went wrong!'));
}