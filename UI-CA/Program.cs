// See https://aka.ms/new-console-template for more information

using Microsoft.EntityFrameworkCore;
using SC.BL;
using SC.DAL;
using SC.DAL.EF;
using SC.UI.CA;

var dbOptionsBuilder = new DbContextOptionsBuilder<SupportCenterDbContext>();
dbOptionsBuilder.UseSqlite("Data Source=SupportCenterDb_EFCodeFirst.db");
SupportCenterDbContext ctx = new SupportCenterDbContext(dbOptionsBuilder.Options);
ITicketRepository repo = new TicketRepository(ctx);
ITicketManager mgr = new TicketManager(repo);

if (ctx.CreateDatabase(dropDatabase: false))
    DataSeeder.Seed(ctx);

ConsoleUi consoleUi = new ConsoleUi(mgr);
consoleUi.Run();
